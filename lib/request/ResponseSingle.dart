class ResponseSingle {
  final int result;
  final String errorDescription;
  final Map<String, dynamic> objectResponse;

  ResponseSingle({this.result, this.errorDescription, this.objectResponse});

  factory ResponseSingle.fromJson(Map<String, dynamic> json) {
    return ResponseSingle(
      result: json['result'],
      errorDescription: json['errorDescription'],
      objectResponse: json['objectResponse'],
    );
  }
}
