class ResponseList {
  final int result;
  final String errorDescription;
  final List<dynamic> objectResponse;

  ResponseList({this.result, this.errorDescription, this.objectResponse});

  factory ResponseList.fromJson(Map<String, dynamic> json) {
    return ResponseList(
      result: json['result'],
      errorDescription: json['errorDescription'],
      objectResponse: json['objectResponse'],
    );
  }
}