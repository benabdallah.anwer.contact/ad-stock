import 'dart:async';

import '../../../utils/constants.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import '../../utils/SizeConfig.dart';



class SplashScreen extends StatefulWidget {
  bool isAuthenticated;
  SplashScreen({Key key,  this.isAuthenticated}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return _StateSplash();
  }
}

class _StateSplash extends State<SplashScreen> with TickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    //initialise size config
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: appBackgroundColor,
      body: SafeArea(
        child:StreamBuilder(
          stream: Connectivity().onConnectivityChanged,
          builder: (BuildContext context,
              AsyncSnapshot<ConnectivityResult> snapshot) {
            if (snapshot != null &&
                snapshot.hasData &&
                snapshot.data != ConnectivityResult.none) {
              Future.delayed(
                  Duration(
                    milliseconds: 4005,
                  ), () {
                if (widget.isAuthenticated) {
                  Navigator.pushReplacementNamed(context, '/Login');
                } else {
                  Navigator.pushReplacementNamed(context, '/Home');
                }
              });
              return  Stack(
                children: [
                  Align(
                    alignment: Alignment.center,
                    child: Image.asset("assets/images/logo.png"),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 30),
                    child: Align(
                        alignment: Alignment.bottomCenter,
                        child: CircularProgressIndicator(
                          color: kPrimaryColor,
                        )
                    ),
                  ),
                ],
              );
            } else {
              return   Stack(
                children: [
                  Align(
                    alignment: Alignment.center,
                    child: Image.asset("assets/images/logo.png"),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 30),
                    child:  Align(
                      alignment: Alignment.bottomCenter,
                      child: Lottie.asset(
                          'assets/images/lost connection.json',
                          width: 110,
                          height: 110
                      ),
                    ),
                  ),
                ],
              );
            }
          },
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}

