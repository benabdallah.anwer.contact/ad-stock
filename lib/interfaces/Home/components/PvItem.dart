import 'dart:convert';


import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';


import '../../../models/pv/PointVente.dart';
import '../../../utils/SizeConfig.dart';
import '../../../utils/constants.dart';
import '../../PV/PVScreen.dart';
class PvItem extends StatefulWidget {
  PointVente pv;
  PvItem(this.pv);


  @override
  _PvItemState createState() => _PvItemState();
}

class _PvItemState extends State<PvItem> {
  @override
  Widget build(BuildContext context) {
    return  GestureDetector(
      onTap: () => {
        FocusScope.of(context).unfocus(),
      Navigator.push(
      context,
      MaterialPageRoute(
      builder: (context) => PvScreen(widget.pv)),
      ),
      },
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          width: MediaQuery.of(context).size.width-20,
          height: 100,
          decoration: BoxDecoration(
            color:Colors.white,
            boxShadow: [
              BoxShadow(
                blurRadius: 3,
                color: Color(0x411D2429),
                offset: Offset(0, 1),
              )
            ],
            borderRadius: BorderRadius.circular(16),
          ),
          child: Padding(
            padding: EdgeInsetsDirectional.fromSTEB(8, 8, 8, 8),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(0, 1, 1, 1),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(12),
                    child: Image.asset(
                      'assets/images/shop.png',
                      width: 70,
                      height: 100,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: EdgeInsetsDirectional.fromSTEB(8, 8, 4, 0),
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.pv!=null&&widget.pv.designation!=null?widget.pv.designation:"designation",
                          style:
                          TextStyle( fontFamily: 'Lexend Deca',
                            color: Color(0xFF090F13),
                            fontSize: 20,
                            fontWeight: FontWeight.w500,)
                        ),
                        Expanded(
                          child: Padding(
                            padding:
                            EdgeInsetsDirectional.fromSTEB(0, 4, 8, 0),
                            child: Text(
                               widget.pv.adresse,
                              textAlign: TextAlign.start,
                              style: TextStyle( fontFamily: 'Lexend Deca',
                                color: Color(0xFF57636C),
                                fontSize: 14,
                                fontWeight: FontWeight.normal,)
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Padding(
                      padding: EdgeInsetsDirectional.fromSTEB(0, 4, 0, 0),
                      child: Icon(
                        Icons.chevron_right_rounded,
                        color: Color(0xFF57636C),
                        size: 24,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsetsDirectional.fromSTEB(0, 0, 4, 8),
                      child: Text(
                        ' ',
                        textAlign: TextAlign.end,
                        style:
                        TextStyle(fontFamily: 'Lexend Deca',
                          color: Color(0xFF4B39EF),
                          fontSize: 14,
                          fontWeight: FontWeight.w500,)

                        ),
                      ),

                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

