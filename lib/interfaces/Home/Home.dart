
import 'package:ad_stock/models/User/partenaire_Bprice.dart';
import 'package:ad_stock/request/ResponseSingle.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../models/pv/PointVente.dart';
import '../../service/api_partner.dart';
import '../../utils/SizeConfig.dart';
import '../../utils/constants.dart';
import 'components/PvItem.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<PointVente> responseListPVs =[];

PartenaireBprice partner;

  Future<void> getPvs()async{
    List<PointVente> p=
    await getListPvs();
    setState(() {
      responseListPVs=p;
    });

  }
  Future<void> getPartner()async{

   ResponseSingle responseSingle=await getPartnerBprice();
   PartenaireBprice p=PartenaireBprice.fromJson(responseSingle.objectResponse);
    setState(() {
      partner=p;
    });

  }

  @override
  void initState() {
    // TODO: implement initState
    getPvs();
    getPartner();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    final Color inActiveIconColor = Color(0xFFB6B6B6);
    //initialise size config
    SizeConfig().init(context);
    return SafeArea(
        child: Scaffold(
          appBar: AppBar(
              backgroundColor: kPrimaryColor,
              title: Text(
                partner!=null&&partner.abbreviation!=null?partner.abbreviation:"",
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              actions: [
                Container(
                  margin: EdgeInsets.all(getProportionateScreenWidth(7)),
                  decoration: BoxDecoration(
                      color: appBarIconColor.withOpacity(0.16),
                      borderRadius: BorderRadius.all(Radius.circular(8))),
                  child: IconButton(
                    onPressed: () async {
                    //deconnecter
                      //clear shared prefs
                      SharedPreferences preferences = await SharedPreferences.getInstance();
                      preferences.clear();
                      Navigator.pushReplacementNamed(context, '/Login');

                    },
                    icon: Icon(
                      Icons.logout,
                    ),
                  ),
                )
              ], ),

          body: Container(
          width: getProportionateScreenWidth(SizeConfig.screenWidth),
          height: getProportionateScreenHeight(SizeConfig.screenHeight),

          // ignore: missing_return
          child:
            responseListPVs!=null&&responseListPVs != []
                ? ListView.builder(
                shrinkWrap: true,
                itemCount: responseListPVs.length,
                // ignore: missing_return
                itemBuilder: (context, position) {
                  //print(responseListPVs[position].designation);
                  PointVente pv =
                  responseListPVs[position];
                  return PvItem( responseListPVs[position]);
                }):Text("empty"),

          ),

        ),
    );
    }
}
