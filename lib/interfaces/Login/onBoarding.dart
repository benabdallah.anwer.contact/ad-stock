
import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:lottie/lottie.dart';
import '../../utils/constants.dart';
import '../Home/Home.dart';

class OnBoardingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) => SafeArea(
    child: IntroductionScreen(
      pages: [
        PageViewModel(
          title: 'Mise à jour de donnèes',
          body: "Secouez votre téléphone pour mettre à jour vos données et l'état des commandes.",
          image: Lottie.asset(
            'assets/images/shake-phone.json',
          ),
          decoration: getPageDecoration(),
        ),
        PageViewModel(
          title: 'Chercher un produit',
          body: 'Vouz pouvez chercher un produit avec son code à bar',
          image: Lottie.asset(
            'assets/images/position.json',
          ),

          decoration: getPageDecoration(),
        ),

        PageViewModel(
          title: 'Mettre a jour les produits',
          body: "Vouz pouvez mettres a jour les produits .",
          image: Lottie.asset(
            'assets/images/autorisation-animation.json',
          ),

          decoration: getPageDecoration(),
        ),

      ],
      done: Text('Terminè', style: TextStyle(fontWeight: FontWeight.w600,color: kPrimaryColor)),
      onDone: () => goToHome(context),
      showSkipButton: false,
      next: Icon(Icons.arrow_forward,color: kPrimaryColor,),
      dotsDecorator: getDotDecoration(),
      onChange: (index) => print('Page $index selected'),
      globalBackgroundColor: kPrimaryColor,
      skipFlex: 0,
      nextFlex: 0,
       animationDuration: 500,
    ),
  );

  void goToHome(context)async{

      Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (_) => Home()),
      );




  }

  Widget buildImage(String path) =>
      Center(child: Image.asset(path, width: 350));

  DotsDecorator getDotDecoration() => DotsDecorator(
    color: Color(0xFFBDBDBD),
    activeColor: kPrimaryColor,
    size: Size(10, 10),
    activeSize: Size(22, 10),
    activeShape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(24),

    ),
  );

  PageDecoration getPageDecoration() => PageDecoration(
    titleTextStyle: TextStyle(fontSize: 28, fontWeight: FontWeight.bold,color: kPrimaryColor),
    bodyTextStyle: TextStyle(fontSize: 18),
    descriptionPadding: EdgeInsets.all(16).copyWith(bottom: 0),
    imagePadding: EdgeInsets.all(24),
    pageColor: Colors.white,
  );
}