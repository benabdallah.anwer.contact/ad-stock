import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import '../../../utils/SizeConfig.dart';
import 'login_form.dart';


class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding:
          EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20.0)),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: SizeConfig.screenHeight * 0.1),
                Align(
                  alignment: Alignment.center,
                  child: Lottie.asset(
                    'assets/images/livreur.json',
                  ),
                ),
                SizedBox(height: SizeConfig.screenHeight * 0.02),
                LoginForm(),
                SizedBox(height: SizeConfig.screenHeight * 0.04),

                Container(
                  width: 120,
                  height: 120,
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Image.asset('assets/images/logo.png'),
                  ) ,
                )


              ],
            ),
          ),
        ),
      ),
    );
  }
}