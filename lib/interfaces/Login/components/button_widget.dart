import 'package:flutter/material.dart';

import '../../../utils/constants.dart';

class ButtonWidget extends StatelessWidget {
  final String text;
  final VoidCallback onClicked;
  final int typeAutorisation ;

  const ButtonWidget({
    Key key,
    @required this.text,
    @required this.onClicked,
    @required this.typeAutorisation,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => RaisedButton(
    onPressed: (){
    // typeAutorisation==1? checkPermission():Geolocator.openLocationSettings();
    },
    color: kPrimaryColor,
    shape: StadiumBorder(),
    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 16),
    child: Text(
      text,
      style: TextStyle(color: Colors.white, fontSize: 16),
    ),
  );
}