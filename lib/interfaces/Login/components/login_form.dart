
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../components/defaultButton.dart';
import '../../../helper/keyboard.dart';
import '../../../service/api_partner.dart';
import '../../../utils/SizeConfig.dart';
import '../../../utils/constants.dart';
import '../../Home/Home.dart';
import '../onBoarding.dart';

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();
   String email;
   String password;
  bool remember = false;
   String _token;
  Future<void> setToken(String token) async {
    setState(() {
      _token = token;
    });
    //save token to shared prefs
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("token", token);
    //should save this token to data base
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          Stack(
            children: [
              Container(
                margin: EdgeInsets.only(
                    left: getProportionateScreenWidth(5),
                    right: getProportionateScreenWidth(5)),
                height: 60,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(appBorderCiruclar),
                /*  boxShadow: [
                    BoxShadow(
                      color: Colors.black38,
                      blurRadius: 5,
                      offset: const Offset(0, 0),
                    ),
                  ],*/
                ),
              ),
              Container(
                  //  width: SizeConfig.screenHeight*0.6,
                  margin: EdgeInsets.only(
                      left: getProportionateScreenWidth(5),
                      right: getProportionateScreenWidth(5)),
                  child: buildEmailFormField()),
            ],
          ),
          SizedBox(height: getProportionateScreenHeight(30)),
          Stack(
            children: [
              Container(
                margin: EdgeInsets.only(
                    left: getProportionateScreenWidth(5),
                    right: getProportionateScreenWidth(5)),
                height: 60,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(appBorderCiruclar),
                 /* boxShadow: [
                    BoxShadow(
                      color: Colors.black38,
                      blurRadius: 5,
                      offset: const Offset(0, 0),
                    ),
                  ],*/
                ),
              ),
              Container(
                  //  width: SizeConfig.screenHeight*0.6,
                  margin: EdgeInsets.only(
                      left: getProportionateScreenWidth(5),
                      right: getProportionateScreenWidth(5)),
                  child: buildPasswordFormField()),
            ],
          ),
          SizedBox(height: getProportionateScreenHeight(30)),
          Container(
            margin: EdgeInsets.only(
                left: getProportionateScreenWidth(20),
                right: getProportionateScreenWidth(20)),
            child: Row(
              children: [
                Spacer(),
                GestureDetector(
                  onTap: () {
                    //Navigator.pushNamed(context, "/opt");
                  },
                  child: Text(
                    "Mot de passe oublié ?",
                    style: TextStyle(
                      color: appBarIconColor,
                    ),
                  ),
                )
              ],
            ),
          ),
          // FormError(errors: errors),
          SizedBox(height: getProportionateScreenHeight(20)),
          Stack(
            children: [
              Container(
                height: getProportionateScreenHeight(60),
                margin: EdgeInsets.only(
                    left: getProportionateScreenWidth(5),
                    right: getProportionateScreenWidth(5)),
                width: SizeConfig.screenWidth,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(appBorderCiruclar),
                /*  boxShadow: [
                    BoxShadow(
                      color: kPrimaryColor,
                      blurRadius: 5,
                      offset: const Offset(0, 0),
                    ),
                  ],*/
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                    left: getProportionateScreenWidth(5),
                    right: getProportionateScreenWidth(5)),
                width: SizeConfig.screenWidth,
                child: DefaultButton(
                  text: "Se connecter",
                  press: () async {




                    if (_formKey.currentState.validate()) {
                      _formKey.currentState?.save();
                      // if all are valid then go to success screen
                      KeyboardUtil.hideKeyboard(context);
                      var response = await loginPartner(email, password);
                      if (response.result == 1) {
                        SharedPreferences prefs =
                            await SharedPreferences.getInstance();
                        //save id point de vente
                        prefs.setString("idPointVente", response.objectResponse.idPointVente);
                        prefs.setString("idPartner", response.objectResponse.idPartenaire);




                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          backgroundColor: kColorLivred,
                          content: const Text(
                            "Authentification validé avec succès",
                            style: TextStyle(fontWeight: FontWeight.w600),
                          ),
                          duration: const Duration(seconds: 1),
                        ));

                     // if (prefs.getBool('isShowBoarding')==false) {
                     //   Navigator.of(context).pushReplacement(
                     //     MaterialPageRoute(builder: (_) => OnBoardingPage()),
                     //   );
                     //   prefs.setBool("isShowBoarding",true);
                     //
                     // }
                     // else {
                       Navigator.of(context).pushReplacement(
                         MaterialPageRoute(builder: (_) => Home()),
                       );
                     //}

                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          backgroundColor: kColorAnnuled,
                          content: Text(
                            response.errorDescription,
                            style: TextStyle(fontWeight: FontWeight.w600),
                          ),
                          duration: const Duration(seconds: 1),
                        ));
                      }
                    }
                  },
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  TextFormField buildPasswordFormField() {
    return TextFormField(
      obscureText: true,
      cursorColor: kPrimaryColor,
      onSaved: (newValue) => password = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          return "";
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          return "*Veuillez entrer votre mot de passe";
        }
        return null;
      },
      decoration: InputDecoration(
          contentPadding: const EdgeInsets.symmetric(vertical: 20.0),
          border: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.circular(appBorderCiruclar),
          ),
          hintText: "Mot de passe",
          hintStyle:
              TextStyle( color: Colors.black12),
          fillColor: Colors.white,
          filled: true,
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(appBorderCiruclar),
              borderSide: BorderSide(color: Colors.white, width: 3.0)),
          floatingLabelBehavior: FloatingLabelBehavior.always,
          prefixIcon: Icon(
            Icons.lock_open_outlined,
            color: kPrimaryColor,
          )
          ),
    );
  }

  TextFormField buildEmailFormField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      cursorColor: kPrimaryColor,

      onSaved: (newValue) => email = newValue,
      validator: (value) {
        if (value.isEmpty) {
          return "* Veuillez entrer votre login";
        }
        return null;
      },
      decoration: InputDecoration(
          contentPadding: const EdgeInsets.symmetric(vertical: 20.0),
          border: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.circular(appBorderCiruclar),
          ),
          hintText: "Login",
          hintStyle:
              TextStyle( color: Colors.black12),
          fillColor: Colors.white,
          filled: true,
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(appBorderCiruclar),
              borderSide: BorderSide(color: Colors.white, width: 3.0)),
          floatingLabelBehavior: FloatingLabelBehavior.always,
          prefixIcon: Icon(
            Icons.account_circle_outlined,
            color: kPrimaryColor,
          )
          ),
    );
  }
}
