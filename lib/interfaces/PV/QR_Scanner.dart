
import 'dart:developer';
import 'dart:io';

import 'package:ad_stock/models/pv/PointVente.dart';
import 'package:ad_stock/request/ResponseSingle.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

import '../../models/produit/produit.dart';
import '../../service/api_product.dart';
import '../../utils/constants.dart';
import '../Produit/ProductScreen.dart';

class ProductScanner extends StatefulWidget {
 // const ProductScanner({Key key}) : super(key: key);
  PointVente  pv;
  ProductScanner(this.pv);
  @override
  State<StatefulWidget> createState() => _ProductScannerState();
}

class _ProductScannerState extends State<ProductScanner> {
  Barcode result;
  QRViewController controller;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
bool scanned;
Produit scannedProduct;
@override
  void initState() {
    // TODO: implement initState
  this.setState(() {
    scanned=false;
  });
    super.initState();
  }
  // In order to get hot reload to work we need to pause the camera if the platform
  // is android, or resume the camera if the platform is iOS.
  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller.pauseCamera();
    } else if (Platform.isIOS) {
      controller.resumeCamera();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Expanded(
            flex: 5,
            child: QRView(
              key: qrKey,
              onQRViewCreated: _onQRViewCreated,
            ),
          ),
          Expanded(
            flex: 1,
            child: Center(
              child: (result != null)
                  ? Text(
                  'Barcode Type: ${describeEnum(result.format)}   Data: ${result.code}')
                  : Text('scanner le code à barre ici'),
            ),
          )
        ],
      ),
    );
  }


  void _onQRViewCreated(QRViewController controller) async{
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) async {

      setState(() {
        result = scanData;

      });
      //fetch data by result
     //await getProduct();

      if(!this.scanned) {
        setState(() {
          scanned=true;

        });
        ResponseSingle responseSingle=await findByQrAndPv(result.code,widget.pv.idPointVente);
        if(responseSingle.result==1)
        {
          Produit produit=Produit.fromJson(responseSingle.objectResponse);
          print(produit.toString());
          setState(() {
            scannedProduct=produit;
          });
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => ProductScreen(produit,widget.pv)),
          );
        }
       else{

          showDialog<String>(
            context: context,
            builder: (BuildContext context) => AlertDialog(
              title: const Text('Produit inexistant'),
              content: const Text('le produit scanné n existe pas'),
              actions: <Widget>[
                TextButton(
                  onPressed: () => Navigator.pop(context),
                  child: const Text('Annuler'),
                ),
                TextButton(
                  onPressed: () => Navigator.pop(context),
                  child: const Text('OK'),
                ),
              ],
            ),
          );
        }

      }

    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }
}

  void _onPermissionSet(BuildContext context, QRViewController ctrl, bool p) {
    log('${DateTime.now().toIso8601String()}_onPermissionSet $p');
    if (!p) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('no Permission')),
      );
    }
  }



