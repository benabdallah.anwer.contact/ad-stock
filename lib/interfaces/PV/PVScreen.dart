import 'package:flutter/material.dart';

import '../../models/pv/PointVente.dart';
import '../../utils/SizeConfig.dart';
import '../../utils/constants.dart';
import 'QR_Scanner.dart';


class PvScreen extends StatefulWidget {
  PointVente pv;
  PvScreen(this.pv);
  @override
  _PvScreenState createState() => _PvScreenState();
}

class _PvScreenState extends State<PvScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
      appBar: AppBar(
      backgroundColor: kPrimaryColor,
      title: Text(
        widget.pv.designation,
        style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
      ),
      actions: [

      ], ),
    body: Container(
    child:  ProductScanner(widget.pv),
    ),
    ),
    );
  }
}
