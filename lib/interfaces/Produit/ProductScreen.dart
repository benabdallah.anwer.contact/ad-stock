import 'package:ad_stock/models/produit/productPvDto.dart';
import 'package:ad_stock/models/produit/produit.dart';
import 'package:ad_stock/models/pv/PointVente.dart';
import 'package:ad_stock/request/ResponseSingle.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../service/api_product.dart';
import '../../utils/SizeConfig.dart';
import '../../utils/constants.dart';

class ProductScreen extends StatefulWidget {
  //change to product
  Produit product;
  PointVente pv;
  ProductScreen(this.product,this.pv);
  @override
  _ProductScreenState createState() => _ProductScreenState();
}

class _ProductScreenState extends State<ProductScreen> {
  TextEditingController stockController;
  TextEditingController prixController;

  bool stockReadOnly = true;
  bool prixHTTCReadOnly = true;

  @override
  void initState() {
    // TODO: implement initState
    stockController = TextEditingController(text:widget.product.produitPointVente.stockReel.toString());
    prixController = TextEditingController(text: widget.product.produitPointVente.prix.toString());

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: kPrimaryColor,
          title: Text(
            widget.product.designation,
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          ),
          actions: [],
        ),
        body:
        SingleChildScrollView(

    child: ConstrainedBox(
    constraints: BoxConstraints(maxHeight: SizeConfig.screenHeight-80),

      child:
        Container(
          width: double.infinity,
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          child: Padding(
            padding: EdgeInsetsDirectional.fromSTEB(0, 12, 0, 24),

              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  // Generated code for this ProPlan Widget...
                  Container(
                    width: double.infinity,
                    height: 100,

                    child: Padding(
                      padding: EdgeInsetsDirectional.fromSTEB(8, 8, 8, 8),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Image.network(
                            widget.product.urlImg!=""?widget.product.urlImg:"",
                            width: 90,
                            height: 90,
                            fit: BoxFit.cover,
                          ),
                          Expanded(
                            child: Padding(
                              padding: EdgeInsetsDirectional.fromSTEB(16, 0, 0, 0),
                              child: Column(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    widget.product.designation,
                                    style: TextStyle(
                                      fontFamily: 'Lexend Deca',
                                      color: Color(0xFF090F13),
                                      fontSize: 20,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsetsDirectional.fromSTEB(0, 4, 0, 0),
                                    child: Text(
                                      widget.pv.designation,
                                      style: TextStyle( fontFamily: 'Lexend Deca',
                                        color: Color(0xFF57636C),
                                        fontSize: 12,
                                        fontWeight: FontWeight.normal,),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),

                        ],
                      ),
                    ),
                  ),

                  // prix httc
                  Container(
                    width: double.infinity,
                    height: 60,
                    decoration: BoxDecoration(
                      color: Colors.white,
                    ),
                    child: Padding(
                      padding: EdgeInsetsDirectional.fromSTEB(12, 8, 12, 8),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        children: [

                          Expanded(
                            child: Padding(
                              padding:
                              EdgeInsetsDirectional.fromSTEB(12, 0, 0, 0),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  // Generated code for this TextField Widget...
                                  Container(
                                    child: TextFormField(
                                      controller: prixController,
                                      keyboardType: TextInputType.number,
                                      readOnly: prixHTTCReadOnly,
                                      autofocus: true,
                                      obscureText: false,
                                      decoration: InputDecoration(
                                        labelText:"Prix",
                                        labelStyle: const TextStyle(color:Color(0xFF57636C),fontSize: 20 ),
                                        suffix: InkWell(
                                          onTap: () => {
                                            setState(() {
                                              prixHTTCReadOnly = !prixHTTCReadOnly;
                                            })
                                          },
                                          child:    const Icon(
                                            Icons.mode_edit,
                                            color: Color(0xFF57636C),
                                            size: 20,
                                          ),
                                        ),
                                        hintText: '[Prix du produit Point de vente...]',
                                        enabledBorder: const OutlineInputBorder(
                                          borderSide: BorderSide(
                                            color: Colors.black,
                                            width: 1,
                                          ),
                                          borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(4.0),
                                            topRight: Radius.circular(4.0),
                                            bottomRight:Radius.circular(4.0) ,
                                            bottomLeft: Radius.circular(4.0),
                                          ),
                                        ),
                                        focusedBorder: const OutlineInputBorder(
                                          borderSide: BorderSide(
                                            color: Colors.black,
                                            width: 1,
                                          ),
                                          borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(4.0),
                                            topRight: Radius.circular(4.0),
                                            bottomRight:Radius.circular(4.0) ,
                                            bottomLeft: Radius.circular(4.0),
                                          ),
                                        ),
                                      ),

                                      style: const TextStyle(
                                        fontFamily: 'Lexend Deca',
                                        color: Color(0xFF262D34),
                                        fontSize: 16,
                                        fontWeight: FontWeight.normal,
                                      ),
                                    ),
                                    width: SizeConfig.screenWidth -
                                        getProportionateScreenWidth(40),
                                    height: getProportionateScreenWidth(80),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  //stock input field
                  Container(
                    width: double.infinity,
                    height: 60,
                    decoration: BoxDecoration(
                      color: Colors.white,
                    ),
                    child: Padding(
                      padding: EdgeInsetsDirectional.fromSTEB(12, 8, 12, 8),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        children: [

                          Expanded(
                            child: Padding(
                              padding:
                              EdgeInsetsDirectional.fromSTEB(12, 0, 0, 0),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  // Generated code for this TextField Widget...
                                  Container(
                                    child: TextFormField(
                                      controller: stockController,
                                      keyboardType: TextInputType.number,
                                      readOnly: stockReadOnly,
                                      autofocus: true,
                                      obscureText: false,
                                      decoration: InputDecoration(
                                        labelText:"Stock (${widget.product.mesure})",
                                        labelStyle: const TextStyle(color:Color(0xFF57636C),fontSize: 20 ),
                                        suffix: InkWell(
                                          onTap: () => {
                                            setState(() {
                                              stockReadOnly = !stockReadOnly;
                                            })
                                          },
                                          child:    const Icon(
                                            Icons.mode_edit,
                                            color: Color(0xFF57636C),
                                            size: 20,
                                          ),
                                        ),
                                        hintText: '[Stock Reel du produit ...]',
                                        enabledBorder: const OutlineInputBorder(
                                          borderSide: BorderSide(
                                            color: Colors.black,
                                            width: 1,
                                          ),
                                          borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(4.0),
                                            topRight: Radius.circular(4.0),
                                            bottomRight:Radius.circular(4.0) ,
                                            bottomLeft: Radius.circular(4.0),
                                          ),
                                        ),
                                        focusedBorder: const OutlineInputBorder(
                                          borderSide: BorderSide(
                                            color: Colors.black,
                                            width: 1,
                                          ),
                                          borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(4.0),
                                            topRight: Radius.circular(4.0),
                                            bottomRight:Radius.circular(4.0) ,
                                            bottomLeft: Radius.circular(4.0),
                                          ),
                                        ),
                                      ),

                                      style: const TextStyle(
                                        fontFamily: 'Lexend Deca',
                                        color: Color(0xFF262D34),
                                        fontSize: 16,
                                        fontWeight: FontWeight.normal,
                                      ),
                                    ),
                                    width: SizeConfig.screenWidth -
                                        getProportionateScreenWidth(40),
                                    height: getProportionateScreenWidth(80),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  new Spacer(),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: // Generated code for this Column Widget...
                    Padding(
                      padding: EdgeInsetsDirectional.fromSTEB(20, 20, 20, 20),
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Padding(
                            padding: EdgeInsetsDirectional.fromSTEB(0, 16, 0, 0),
                            child: InkWell(
                              onTap: () async{
                                print('Button pressed ...');
                                productPvDto productPV= productPvDto(widget.pv.idPointVente , widget.product.idProduit,
                                    widget.product.produitPointVente.idproduitPointVente,
                                    double.parse(prixController.text), double.parse(stockController.text));

                                ResponseSingle response=await UpdateProduit(productPV);
                                print(response.result);
                                if(response.result==1)
                                {
                                  Navigator.pop(context);

                                  //  stockController.text=response.objectResponse["stockReel"];
                                  // prixController.text=response.objectResponse["prix"];
                                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                    backgroundColor: kColorLivred,
                                    content:  Text(
                                      response.errorDescription,
                                      style: const TextStyle(fontWeight: FontWeight.w600),
                                    ),
                                    duration: const Duration(seconds: 1),
                                  ));

                                }
                                else{
                                  print("failed to update");
                                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                    backgroundColor: kColorAnnuled,
                                    content:  Text(
                                      response.errorDescription,
                                      style: TextStyle(fontWeight: FontWeight.w600),
                                    ),
                                    duration: const Duration(seconds: 1),
                                  ));
                                }
                              },
                              child: Container(
                                height: 60,
                                width: SizeConfig.screenWidth/2,
                                decoration: BoxDecoration(
                                  color:Color(0xFF10a8a8),

                                  boxShadow: const [
                                    BoxShadow(
                                      blurRadius: 3,
                                      color: Color(0x411D2429),
                                      offset: Offset(0, 1),
                                    )
                                  ],
                                  borderRadius: BorderRadius.circular(40),
                                ),

                                child: Center(
                                  child: Text('Confirmer',style: TextStyle(
                                    fontFamily: 'Lexend Deca',
                                    color: Colors.white,
                                    fontSize: 16,
                                    fontWeight: FontWeight.normal,
                                  )),
                                ),




                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsetsDirectional.fromSTEB(0, 16, 0, 0),
                            child: InkWell(
                              onTap: () {
                                print('Button pressed ...');
                                Navigator.pop(context);

                              },
                              child: Container(
                                height: 60,
                                width: SizeConfig.screenWidth/2,

                                decoration: BoxDecoration(
                                  color:Color(0xFFE06666),

                                  boxShadow: const [
                                    BoxShadow(
                                      blurRadius: 3,
                                      color: Color(0x411D2429),
                                      offset: Offset(0, 1),
                                    )
                                  ],
                                  borderRadius: BorderRadius.circular(40),
                                ),

                                child: Center(
                                  child: Text('Annuler',style: TextStyle(
                                    fontFamily: 'Lexend Deca',
                                    color: Colors.white,
                                    fontSize: 16,
                                    fontWeight: FontWeight.normal,
                                  )),
                                ),




                              ),
                            ),
                          ),

                        ],
                      ),
                    )
                    ,
                  )
                ],
              ),

          ),
        ),
      ),
      ),
      ),
    );
  }
}
