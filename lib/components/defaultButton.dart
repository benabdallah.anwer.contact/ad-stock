import 'package:flutter/material.dart';

import '../utils/SizeConfig.dart';
import '../utils/constants.dart';

class DefaultButton extends StatelessWidget {
  const DefaultButton({
    Key key,
    this.text,
    this.press,
  }) : super(key: key);
  final String text;
  final Function press;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: getProportionateScreenWidth(260),
      height: getProportionateScreenHeight(60),
      child: FlatButton(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(appBorderCiruclar)),
          color: kPrimaryColor,
          onPressed: press,
          child: Column(
            children: [
              SizedBox(height: (getProportionateScreenHeight(60)-getProportionateScreenWidth(20))/2),
              Text(
                text,
                style: TextStyle(
                  fontSize: getProportionateScreenWidth(17),
                  color: Colors.white,
                ),
              ),

            ],
          )
      ),
    );
  }
}
