import 'dart:convert';


import 'package:ad_stock/request/ResponseSingle.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import '../models/User/User.dart';
import '../models/pv/PointVente.dart';
import '../request/ResponseList.dart';
import '../utils/links.dart';



Future <ResponseSingle>  getPartnerBprice()async{
  SharedPreferences preferences = await SharedPreferences.getInstance();
String idPartenaire=preferences.getString("idPartner");


  String url =
      "$ServerPrefix/bp-api-pos/v1/findByIdPartenaire/$idPartenaire";
  var response = (await http.get(
    Uri.parse(url),
    headers: <String, String>{
      'Content-Type': 'application/json',
    },

  ));

  return ResponseSingle.fromJson(jsonDecode(response.body));
}

Future <ResponseSingleUser>  loginPartner(String phone,String password)async{


  String url =
      "$ServerPrefix/bp-api-admin/v1/Authentification";
  var response = (await http.post(
    Uri.parse(url),
    headers: <String, String>{
      'Content-Type': 'application/json',
    },
    body:
    jsonEncode(<String, String>{ "login": phone,
      "password": password,
      "typeconnection": "propr" }),
  ));

  return ResponseSingleUser.fromJson(jsonDecode(response.body));
}

Future<List<PointVente>>  getListPvs()async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  var idPartner = prefs.getString("idPartner");
  String url =
      "$ServerPrefix/bp-api-pos/v1/findAllByIdPartenaireBpriceAndFActif/$idPartner/1";
  var response = (await http.get(
    Uri.parse(url),
    headers: <String, String>{
      'Content-Type': 'application/json;'
    },

  ));
  print(json.decode(response.body));
  ResponseList responseList =
  ResponseList.fromJson(json.decode(response.body));
  List<PointVente> listPvs = [];
  if (responseList.result == 1 && responseList.objectResponse != null) {
    listPvs = List.from(responseList.objectResponse)
        .map((e) =>PointVente.fromJson(e) )


        .toList();
  }
 //print(PointVente.listFromJson(ResponseList.fromJson(jsonDecode(response.body)).objectResponse).toString());
  return listPvs;
}





