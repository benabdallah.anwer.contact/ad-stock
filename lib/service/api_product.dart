

import 'dart:convert';


import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import '../models/produit/productPvDto.dart';
import '../models/produit/produit.dart';
import '../request/ResponseSingle.dart';
import '../utils/links.dart';

Future<ResponseSingle> getAllProducts(
    String type, String idPartner) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String idPartner = prefs.getString("idPartner");

  String url =
      "$ServerPrefix/bp-api-product//v1/findAllByTypeProduit/$type/$idPartner";
  ResponseSingle response = (await http.get(
    Uri.parse(url),
    headers: <String, String>{
      'Content-Type': 'application/json',
    },
  )) as ResponseSingle;
  return response;
}


Future<ResponseSingle> UpdateProduit(
    productPvDto produitPvDto) async {
  String url = "$ServerPrefix/bp-api-product/v1/updateProduitProintVente";
 var response= await http.post(
    Uri.parse(url),
    headers: <String, String>{
      'Content-Type': 'application/json',
    },
    body: jsonEncode(<String, dynamic>{
      "idPointVente": produitPvDto.idPointVente,
      "idProduit": produitPvDto.idProduit,
      "idproduitPointVente": produitPvDto.idproduitPointVente,
      "prix": produitPvDto.prix,
      "stockReel": produitPvDto.stockReel,
    }),
  );
  ResponseSingle responseSingle=ResponseSingle.fromJson(jsonDecode(response.body));
return responseSingle;
}


Future<ResponseSingle> findByQrAndPv(
    String qrCode,String idPv) async {
  print("find product called");
  SharedPreferences prefs = await SharedPreferences.getInstance();

  String url =
      "$ServerPrefix/bp-api-product/v1/findAllByIdPointVenteAndCodeBarre/$idPv/$qrCode";
  var response = (await http.get(
    Uri.parse(url),
    headers: <String, String>{
      'Content-Type': 'application/json',
    },
  )) ;
  print(jsonDecode(response.body));
  ResponseSingle responseSingle=ResponseSingle.fromJson(jsonDecode(response.body));
  return responseSingle;
}
