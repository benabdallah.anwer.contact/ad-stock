import 'dart:convert';

UserObjectResponse userObjectResponseFromJson(String str) =>
    UserObjectResponse.fromJson(json.decode(str));
String userObjectResponseToJson(UserObjectResponse data) => json.encode(data.toJson());

class UserObjectResponse {
  UserObjectResponse({
    this.idUtilisateur,
    this.idPointVente,
    this.idPartenaire,

  });

  String idUtilisateur;
  String idPointVente;
  String idPartenaire;


  factory UserObjectResponse.fromJson(Map<String, dynamic> json) =>
      UserObjectResponse(
        idUtilisateur: json["idUtilisateur"],
        idPointVente: json["idPointVente"],
        idPartenaire: json["idPartenaire"],

      );

  Map<String, dynamic> toJson() => {
    "idUtilisateur": idUtilisateur,
    "idPartenaire": idPartenaire,
    "idPointVente": idPointVente,


  };
}


