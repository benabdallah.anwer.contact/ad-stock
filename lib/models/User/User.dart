
import 'UserObjectResponse.dart';

class ResponseSingleUser {
  int result;
  String errorDescription;
  UserObjectResponse objectResponse;


  ResponseSingleUser({this.result, this.errorDescription, this.objectResponse});

  factory ResponseSingleUser.fromJson(Map<String, dynamic> json) {
    return ResponseSingleUser(
      result: json['result'],
      errorDescription: json['errorDescription'],
      objectResponse: UserObjectResponse.fromJson(json['objectResponse']),
    );
  }

}
