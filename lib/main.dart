
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'interfaces/Home/Home.dart';
import 'interfaces/Login/Login.dart';
import 'interfaces/Splash/SplashSceen.dart';



Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  //get id user and test if logged in
  SharedPreferences prefs = await SharedPreferences.getInstance();
  var idPartner = prefs.getString("idPartner");



  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown])
      .then((_){
    runApp(
        MaterialApp(
            title: "AdCaisseAppStock",
            debugShowCheckedModeBanner: false,
            routes: <String, WidgetBuilder>{
              '/Login': (BuildContext context) => SafeArea(child: Login()),
              '/Home': (BuildContext context) => SafeArea(child: Home()),
            },
            home: idPartner == null ?
            SplashScreen(isAuthenticated:true):SplashScreen(isAuthenticated:false))
    );
  }
  );
}
